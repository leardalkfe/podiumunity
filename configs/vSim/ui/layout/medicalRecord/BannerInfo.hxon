{
    name: "BannerInfo",
    props: [
        {
			name : "bannerInfoSemantic",
			type : HxonTypeNamed("formulas.externalTypes.SemanticScopeRef"),
			defaultValue : "",
			description : "Reference to the semantic layer of the banner info"
		},
        {
			name : "cellShort",
			type : HxonTypeInt,
			defaultValue : "86",
			description : "Describes the width of the cells in the banner"
		},
        {
			name : "cellLong",
			type : HxonTypeInt,
			defaultValue : "215",
			description : "Describes the width of the cells in the banner"
		},
        {
			name : "cellHeader",
			type : HxonTypeInt,
			defaultValue : "500",
			description : "Describes the width of the cells in the banner"
		},
        {
			name : "firstColumn",
			type : HxonTypeFloat,
			defaultValue : "56",
			description : "Placement of first column of info"
		},
        {
			name : "secondColumn",
			type : HxonTypeFloat,
			defaultValue : "582",
			description : "Placement of second column of info"
		},
        {
			name : "thirdColumn",
			type : HxonTypeFloat,
			defaultValue : "928",
			description : "Placement of third column of info"
		},
        {
			name : "fourthColumn",
			type : HxonTypeFloat,
			defaultValue : "1202",
			description : "Placement of fourth column of info"
		},
        {
			name : "firstRow",
			type : HxonTypeFloat,
			defaultValue : "12",
			description : "Placement of first row of info"
		},
        {
			name : "secondRow",
			type : HxonTypeFloat,
			defaultValue : "52",
			description : "Placement of second row of info"
		},
        {
			name : "rowHeight",
			type : HxonTypeFloat,
			defaultValue : "34",
			description : "Height of the labels"
		},
        {
			name : "space",
			type : HxonTypeFloat,
			defaultValue : "15",
			description : "Space inbetween elements"
		},
    ],
    element: Canvas({
        elements : [
            {   //First row
                //visible : e('true'),
                x       : e('firstColumn'), 
                y       : e('firstRow'),
                width   : e('cellHeader'),
                height  : e('rowHeight'), 
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientName.value') },
                        { name : "fontSize", value : e('32.0') },
                        { name : "fontType", value : e('"bold"') },
                        { name : "testName", value : e('"PatientName"')}
                    ]
                }),
            },
            {   //Gender info
                x       : e('secondColumn'), 
                y       : e('firstRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element: Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientGender.value') },
                    ]
                })
            },
            {   
                x       : e('secondColumn + space + cellShort'), 
                y       : e('firstRow'),
                width   : e('cellLong + 20'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.gender.value') },
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
            {   //Height Info
                x       : e('thirdColumn'), 
                y       : e('firstRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientHeight.value') },
                    ]
                })
            },
            {   
                x       : e('thirdColumn + space + cellShort'), 
                y       : e('firstRow'),
                width   : e('cellLong - 40'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.height.value')},
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
            {   //Allergies Info
                x       : e('fourthColumn'), 
                y       : e('firstRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientAllergies.value') },
                    ]
                })
            },
            {   
                x       : e('fourthColumn + space + cellShort'), 
                y       : e('firstRow'),
                width   : e('cellLong - 40'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.allergies.value') },
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },  //Second row
            {   //Adm DX Info
                x       : e('firstColumn'), 
                y       : e('secondRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientDiagnosis.value') },
                    ]
                })
            },
            {   
                x       : e('firstColumn + space + cellShort'), 
                y       : e('secondRow'),
                width   : e('400'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.diagnosis.value') },
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
            {   //DOB Info
                x       : e('secondColumn'), 
                y       : e('secondRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientDateOfBirth.value') },
                    ]
                })
            },
            {   
                x       : e('secondColumn + space + cellShort'), 
                y       : e('secondRow'),
                width   : e('cellLong + 20'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [ //TODO: concatenate two inputs
                        { name : "labelText", value : e('bannerInfoSemantic.dateOfBirthString.value') },
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
            {   //Weight Info
                x       : e('thirdColumn'), 
                y       : e('secondRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.patientWeight.value')},
                    ]
                })
            },
            {   
                x       : e('thirdColumn + space + cellShort'), 
                y       : e('secondRow'),
                width   : e('cellLong - 40'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.weight.value')},
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
            {   //Adm On Info
                x       : e('fourthColumn'), 
                y       : e('secondRow'),
                width   : e('cellShort'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.admissionDate.value') },
                    ]
                })
            },
            {   
                x       : e('fourthColumn + space + cellShort'), 
                y       : e('secondRow'),
                width   : e('cellLong - 40'),
                height  : e('rowHeight'),
                element : Widget({ 
                    type: "BannerLabel",
                    props: [
                        { name : "labelText", value : e('bannerInfoSemantic.admDate.value') },
                        { name : "fontType", value : e('"bold"') },
                    ]
                })
            },
        ]
    })
}
                
               /* 
                Split({
                    direction: e('layout.Direction.Down'),
                    margins: e('{ top: 10.0, right: 10.0, bottom: 10.0, left: sideMargin }'),
                    space: e('5'),
                    align: e('layout.Align.First'),
                    elements: [
                        {   // Upper row
                            size: e('50%'),
                            element: Split({
                                direction: e('layout.Direction.Right'),
                                margins: e('{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }'),
                                space: e('10'),
                                align: e('layout.Align.First'),
                                elements: [
                                    {   //Name
                                        size: e('cellHeader'),
                                        element: Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"NamePlaceholder"') },
                                                { name : "fontSize", value : e('30.0') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //Gender info
                                        size: e('cellShort'),
                                        element: Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Gender"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Male"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //Height Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Height"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"175"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //Allergies Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Allergies"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"None"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                ]
                            })
                        },
                        {   // Lower row
                            size: e('1?'),
                            element: Split({
                                direction: e('layout.Direction.Right'),
                                margins: e('{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }'),
                                space: e('5'),
                                align: e('layout.Align.First'),
                                elements: [
                                    {   //Adm DX Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Adm DX"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Acute myocardial infarction"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //DOB Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"DOB"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"7/19/1965 (54y)"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //Weight Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Weight"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"110 kg"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                    {   //Adm On Info
                                        size: e('cellShort'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"Adm On"') },
                                            ]
                                        })
                                    },
                                    {   
                                        size: e('cellLong'),
                                        element : Widget({ 
                                            type: "BannerLabel",
                                            props: [
                                                { name : "labelText", value : e('"12/9/2019"') },
                                                { name : "fontType", value : e('"bold"') },
                                            ]
                                        })
                                    },
                                ]
                            })
                        }
                    ]
                })
            }
        ]
    })
    
    
    
    */
    