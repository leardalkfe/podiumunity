{
    name  : "TimeControl",
	props : [
        {
            name: "isInitialPause",
            type: HxonTypeNamed("formulas.externalTypes.SemanticNodeRef"),
            defaultValue: "",
            description: "Used to disable all but play button in intitial pause"
        },
        {
            name : "timeControlSemanticScope",
            type : HxonTypeNamed("formulas.externalTypes.SemanticScopeRef"),
            defaultValue : "",
            description : "Semantic node representing time control"
        }
    ],
    element: Box({
        width : e("396"),
        height : e("100%"),
        fit : e("layout.Fit.Move(0.0,0.0)"),
        horzScroll : e(""),
        vertScroll : e(""),
        margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
        element : Layers({
            elements: [
                {
                    element : Widget({ 
                        type: "SlicedImage", 
                        props: [
                            { name: "path", value : e('styles.backgrounds.rounded')},
                            { "sliceWidth", e('17')},
                        ]
                    })
                },
                {
                    element : Split({
                        direction : e('layout.Direction.Right'),
                        margins : e("{ top: 0.0, right: 8.0, bottom: 8.0, left: 8.0 }"),
                        space : e('0'),
                        align : e('layout.Align.First'),
                        elements : [
                            {
                                size    : e(''),
                                element : Box({ 
                                    width : e(""),
                                    height : e(""),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 5.0, right: 10.0, bottom: 0.0, left: 20.0 }"),
                                    element : Widget({
                                        type: "Label",
                                        props: [
                                            { name : "text", value : e('timeControlSemanticScope.textSimulationTime.value') },
                                            { "fontSize", e('26')},
                                            { "fontWeight", e('"bold"')},
                                            { "color", e('if(isInitialPause.value, styles.colors.disabledText, styles.colors.default)')}
                                        ],
                                        testPoints: [
                                            {
                                                category: e("'Label'"), location: e("'TimeControl'"), name: e("'SimulationTime'"),
                                                properties: [{e("'Text'"), "text"}]
                                            }
                                        ]
                                    })
                                })
                            },
                            {
                                size    : e('1'),
                                element : Box({ 
                                    width : e("1"),
                                    height : e("24"),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                    element : Widget({
                                        type: "Rect",
                                        props: [
                                            { "color", e('if(isInitialPause.value, styles.colors.disabledText, #9D9D9D)')}
                                        ]
                                    })
                                })
                            },
                            {
                                size    : e(''),
                                element : Box({ 
                                    width : e(""),
                                    height : e(""),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 5.0, right: 10.0, bottom: 0.0, left: 10.0 }"),
                                    element : Widget({
                                        type: "Label",
                                        props: [
                                            { name : "text", value : e('timeControlSemanticScope.simulationDate.value') },
                                            { "fontSize", e('26')},
                                            { "fontWeight", e('"bold"')},
                                            { "color", e('if(isInitialPause.value, styles.colors.disabledText, styles.colors.default)')},
                                            { "textAlign", e('"center"')}
                                        ],
                                        testPoints: [
                                            {
                                                category: e("'Labels'"), location: e("'TimeControl'"), name: e("'SimulationDate'"),
                                                properties: [{e("'Text'"), "text"}]
                                            }
                                        ]
                                    })
                                })
                            },
                            {
                                size    : e('1'),
                                element : Box({ 
                                    width : e("1"),
                                    height : e("24"),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                    element : Widget({
                                        type: "Rect",
                                        props: [
                                            { "color", e('if(isInitialPause.value, styles.colors.disabledText, #9D9D9D)')}
                                        ]
                                    })
                                })
                            },
                            {
                                size    : e(''),
                                element : Box({ 
                                    width : e(""),
                                    height : e(""),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 13.0 }"),
                                    element : Widget({
                                        type: "ControlButton",
                                        props: [
                                            { name : "controlName", value : e('"PlayPause"') },
                                            { name : "semanticNode", value : e('timeControlSemanticScope.togglePlayPause') },
                                            { name : "iconOn", value : e('styles.icons.pause') },
                                            { name : "iconOff", value : e('styles.icons.play') },
                                            { name : "iconDisabled", value : e('styles.icons.playDisabled')},
                                            { name : "disabled", value : e('SemanticMain.medicalRecord.isInitialEHR.value') },
                                            { name : "iconWidth", value : e('32')},
                                            { name : "iconHeight", value : e('32')}
                                            //{ name : "focus", value : e('node.focus') }
                                        ]
                                    })
                                })
                            },
                            {
                                size    : e(''),
                                element : Box({ 
                                    width : e(""),
                                    height : e(""),
                                    fit : e("layout.Fit.Move(0.5,0.5)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 5.0 }"),
                                    element : Widget({
                                        type: "ControlButton",
                                        props: [
                                            { name : "controlName", value : e('"FastForward"') },
                                            { name : "semanticNode", value : e('timeControlSemanticScope.toggleFastForward') },
                                            { name : "iconOn", value : e('styles.icons.fastforward') },
                                            { name : "iconOff", value : e('styles.icons.fastforward') },
                                            { name : "iconDisabled", value : e('styles.icons.fastforwardDisabled')},
                                            { name : "blinkToggled", value : e('true') },
                                            { name : "disabled", value : e('isInitialPause.value') },
                                            { name : "iconWidth", value : e('32')},
                                            { name : "iconHeight", value : e('32')}
                                            //{ name : "focus", value : e('node.focus') }
                                        ]
                                    })
                                })
                            },
                        ]
                    })
                }
            ]
        })
    })
}