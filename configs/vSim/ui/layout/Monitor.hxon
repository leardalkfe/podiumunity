{
    name  : "Monitor",
    props : [
        {
            name : "monitorSemanticScope",
            type : HxonTypeNamed("formulas.externalTypes.SemanticScopeRef"),
            defaultValue : "",
            description : "Semantic node representing the monitor"
        }
    ],
    element: Split({
        direction : e('layout.Direction.Left'),
        margins : e("{ top: 14.0, right: 10.0, bottom: 8.0, left: 16.0 }"),
        space : e('-18'),
        align : e('layout.Align.First'),
        elements : [
            {
                size : e('if (monitorSemanticScope.toggleMonitor.value, 332.0, 128.0)'),
                element : Box({
                    width : e("100%"),
                    height : e("287"),
                    fit : e("layout.Fit.Move(0,0)"),
                    horzScroll : e(""),
                    vertScroll : e(""),
                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                    element : Layers({
                        elements: [
                            {
                                element : Widget({ 
                                    type: "SlicedImage", 
                                    props: [
                                        { name: "path", value : e('styles.backgrounds.monitor')},
                                        { "sliceWidth", e('16')},
                                    ]
                                })
                            },
                            {
                                element : Split({
                                    direction : e('layout.Direction.Right'),
                                    margins : e("{ top: 22.0, right: 30.0, bottom: 0.0, left: 0.0 }"),
                                    space : e('0'),
                                    align : e('layout.Align.Last'),
                                    elements : [
                                        {
                                            size : e("174"),
                                            visible : e('monitorSemanticScope.toggleMonitor.value'),
                                            element : Split({
                                                direction : e('layout.Direction.Down'),
                                                margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                                space : e('0'),
                                                align : e('layout.Align.First'),
                                                elements : [
                                                    {
                                                        size : e("25%"),
                                                        element : Widget({ type: "Graph", props: [
                                                            {"points", e("monitorSemanticScope.graphEcgValues.points")},
                                                            {"color", e("#00FF00")},
                                                            {"plotstyle", e("'line'")},
                                                            {"scrollMode", e("'periodic'")},
                                                            {"lineWidth", e("2.0")},
                                                            {"clearArea", e("0.2")}
                                                        ]})

                                                    }
                                                ]
                                            })
                                        },
                                        {
                                            size : e("76"),
                                            element : Split({
                                                direction : e('layout.Direction.Down'),
                                                margins : e("{ top: 13.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                                space : e('0'),
                                                align : e('layout.Align.First'),
                                                elements : [
                                                    {
                                                        size : e(""),
                                                        element : Widget({
                                                            type: "Label",
                                                            props: [
                                                                { name : "text", value : e('"HR"') },
                                                                { "fontSize", e('22')},
                                                                { "fontWeight", e('900')},
                                                                { "textAlign", e('"right"')},
                                                                { "color", e('styles.colors.green')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Widget({
                                                            type: "Label",
                                                            props: [
                                                                { name : "text", value : e('monitorSemanticScope.textMonitorHeartRate.value') },
                                                                { "fontSize", e('22')},
                                                                { "fontWeight", e('900')},
                                                                { "textAlign", e('"right"')},
                                                                { "color", e('styles.colors.green')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e("22"),
                                                        element : Widget({
                                                            type: "Rect",
                                                            props: [
                                                                { "color", e('styles.colors.transparent')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Widget({
                                                            type: "Label",
                                                            props: [
                                                                { name : "text", value : e('"NIBP"') },
                                                                { "fontSize", e('22')},
                                                                { "fontWeight", e('900')},
                                                                { "textAlign", e('"right"')},
                                                                { "color", e('styles.colors.red')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Widget({
                                                            type: "Label",
                                                            props: [
                                                                { name : "text", value : e('monitorSemanticScope.textMonitorNIBP.value') },
                                                                { "fontSize", e('22')},
                                                                { "fontWeight", e('900')},
                                                                { "textAlign", e('"right"')},
                                                                { "color", e('styles.colors.red')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Widget({
                                                            type: "Label",
                                                            props: [
                                                                { name : "text", value : e('"mmHg"') },
                                                                { "fontSize", e('14')},
                                                                { "fontWeight", e('900')},
                                                                { "textAlign", e('"right"')},
                                                                { "color", e('styles.colors.red')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e("26"),
                                                        element : Widget({
                                                            type: "Rect",
                                                            props: [
                                                                { "color", e('styles.colors.transparent')}
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Split({
                                                            direction : e('layout.Direction.Right'),
                                                            margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                                            space : e('0'),
                                                            align : e('layout.Align.Last'),
                                                            elements : [
                                                                {
                                                                    size: e(""),
                                                                    element: Widget({
                                                                        type: "Label",
                                                                        props: [
                                                                            { name : "text", value : e('"SpO"') },
                                                                            { "fontSize", e('22')},
                                                                            { "fontWeight", e('900')},
                                                                            { "textAlign", e('"right"')},
                                                                            { "color", e('styles.colors.yellow')}
                                                                        ]
                                                                    })
                                                                },
                                                                {
                                                                    size: e(""),
                                                                    element: Box({
                                                                        width : e(""),
                                                                        height : e("100%"),
                                                                        fit : e("layout.Fit.Move(1.0,1.0)"),
                                                                        horzScroll : e(""),
                                                                        vertScroll : e(""),
                                                                        margins : e("{ top: 8.0, right: 0.0, bottom: 0.0, left: -1.0 }"),
                                                                        element: Widget({
                                                                            type: "Label",
                                                                            props: [
                                                                                { name : "text", value : e('"2"') },
                                                                                { "fontSize", e('14')},
                                                                                { "fontWeight", e('900')},
                                                                                { "textAlign", e('"right"')},
                                                                                { "color", e('styles.colors.yellow')}
                                                                            ]
                                                                        })
                                                                    })
                                                                }
                                                            ]
                                                        })
                                                    },
                                                    {
                                                        size : e(""),
                                                        element : Split({
                                                            direction : e('layout.Direction.Right'),
                                                            margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                                            space : e('0'),
                                                            align : e('layout.Align.Last'),
                                                            elements : [
                                                                {
                                                                    size: e(""),
                                                                    element: Widget({
                                                                        type: "Label",
                                                                        props: [
                                                                            { name : "text", value : e('monitorSemanticScope.textMonitorSpO2.value') },
                                                                            { "fontSize", e('22')},
                                                                            { "fontWeight", e('900')},
                                                                            { "textAlign", e('"right"')},
                                                                            { "color", e('styles.colors.yellow')}
                                                                        ]
                                                                    })
                                                                },
                                                                {
                                                                    size: e(""),
                                                                    element: Box({
                                                                        width : e(""),
                                                                        height : e("100%"),
                                                                        fit : e("layout.Fit.Move(1.0,1.0)"),
                                                                        horzScroll : e(""),
                                                                        vertScroll : e(""),
                                                                        margins : e("{ top: 8.0, right: 0.0, bottom: 0.0, left: -1.0 }"),
                                                                        element: Widget({
                                                                            type: "Label",
                                                                            props: [
                                                                                { name : "text", value : e('"%"') },
                                                                                { "fontSize", e('14')},
                                                                                { "fontWeight", e('900')},
                                                                                { "textAlign", e('"right"')},
                                                                                { "color", e('styles.colors.yellow')}
                                                                            ]
                                                                        })
                                                                    })
                                                                }
                                                            ]
                                                        })
                                                    }
                                                ]
                                            })
                                        }
                                    ]
                                })
                            }
                        ]
                    })
                })
            },
            {
                size : e('54'),
                element : Box({ 
                    width : e("54"),
                    height : e("54"),
                    fit : e("layout.Fit.Move(1,0)"),
                    horzScroll : e(""),
                    vertScroll : e(""),
                    margins : e("{ top: 0.0, right: -0.5, bottom: 0.0, left: 0.0 }"),
                    element: Layers({
                        elements: [
                            {
                                element : Widget({ 
                                    type: "SlicedImage", 
                                    props: [
                                        { name: "path", value : e('styles.backgrounds.monitorExpandButton')},
                                        { "sliceWidth", e('16')},
                                    ]
                                })
                            },
                            {
                                element : Box({ 
                                    width : e("10"),
                                    height : e("16"),
                                    fit : e("layout.Fit.Move(0.5,0.4)"),
                                    horzScroll : e(""),
                                    vertScroll : e(""),
                                    margins : e("{ top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 }"),
                                    element : Widget({ 
                                        type: "Image", 
                                        props: [
                                            { name: "path", value : e('if (monitorSemanticScope.toggleMonitor.value, styles.icons.monitorMinimize, styles.icons.monitorExpand)')}
                                        ]
                                    })
                                })
                            },
                            {
                                element : Box({
                                    width : e('100%'),
                                    height : e('100%                    '),
                                    fit : e('layout.Fit.Move(0.5, 0.5)'),
                                    horzScroll : e(''),
                                    vertScroll : e(''),
                                    focusHandler : e('monitorSemanticScope.toggleMonitor.focus'),
                                    margins : e('{ top: 0.0, right: 2.0, bottom: 8.0, left: 0.0 }'),
                                    element : Widget({ 
                                        type: "Interaction", 
                                        props: [
                                            {"id", e('"toggleMonitor"')},
                                            {"press", e('monitorSemanticScope.toggleMonitor.press')}, 
                                            {"onHover", e('monitorSemanticScope.toggleMonitor.hover')}
                                        ] 
                                    })
                                })
                            }
                        ]
                    })
                })
            }
        ]
    })
}