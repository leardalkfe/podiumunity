{
    labtests : [
        {
            time : { hour : 8, min : 0 },
            category : "venousBloodTestsCompleteBloodCount",
            results : [
                { id : "HbOutPut", value : 6.9 },
                { id : "HCT_v", value : 21.0 },
                { id : "WBC", value : 8.8 },
                { id : "platelets", value : 220.0 },
            ]
        },
        {
            time : { hour : 8, min : 0 },
            category : "venousBloodTestsElectrolytes",
            results : [
                { id : "sodium_v", value : 140.0 },
                { id : "potassiumNoise_v", value : 4.0 },
                { id : "chloride", value : 100.0 },
                { id : "HCO3out_v", value : 25.0 },
                { id : "urea", value : 15.0 },
                { id : "creatinine", value : 1.1 },
                { id : "glucoseVariety_v", value : 105.0 },
            ]
        },
        {
            time : { hour : 8, min : 0 },
            category : "venousBloodTestsMiscellaneous",
            results : [
                { id : "PT", value : 12.0 },
                { id : "INR", value : 1.0 },
                { id : "aPTT", value : 34.0 },
            ]
        }
    ],
    observations : [
        //Fill in observations here. In the legacy patient scripts look for 'setInitialObservationEntry(...' - see example below
        {
            time : { hour : 6, min : 0 },
            initials : "MB",
            observations : [
                { name : "bp", value : "99/67" },
                { name : "heartRate", value : "98" },
                { name : "respRate", value : "16" },
                { name : "spo2", value : "96" },
                { name : "oxygenMode", value : "0" },
                { name : "oxygenRoute", value : "¤RA" },//RA=Room air, NC=Nasal cannula, SM=Simple Mask, NRB=Non-rebreather ???
                { name : "temp", value : "36.9" },
                { name : "tempRoute", value : "¤Tympanic" }
            ]
        },
    ],
    drugOrders : [
        //Fill in drug orders here. In the legacy patient scripts look for 'setOrderedDrug(...' - see example below        
        {
            type : Regular,
            orders : [
                { drugName : "Sagm", label : "¤Packed red blood cells IV at 100 mL/hour", dose: {value: 100.0, unit: "mL/hr"}, route : MEDICINE_INFUSION, orderType: Regular},
                { drugName : "Paracetamol", label : "¤Acetaminophen 1 g PO every 8 hours (0600-1400-2200)", dose: {value: 1000.0, unit: "mg"}, route : ORAL, orderType: Regular},
                { drugName : "Gabapentin", label : "¤Gabapentin 300 mg PO every 8 hours (0600-1400-2200)", dose: {value: 300.0, unit: "mg"}, route : ORAL, orderType: Regular},
            ]
        },    
        {
            type : PRN,
            orders : [
                { drugName : "Oxycodone", label : "¤Oxycodone 5 mg PO every 4 hours PRN moderate pain level 4-7", dose: {value: 5.0, unit: "mg"}, route : ORAL, orderType: PRN},
                { drugName : "Oxycodone10mg", label : "¤Oxycodone 10 mg PO every 4 hours PRN severe pain level 8-10", dose: {value: 10.0, unit: "mg"}, route : ORAL, orderType: PRN},
            ]
        }
    ],

    
    administeredDrugs : [
        //Fill in administered drugs here. In the legacy patient scripts look for 'setInitialDrug('...) - see example below
        { drugName : "Paracetamol", route : ORAL,   dose : { value: 1000.0,  unit : "mg" },   time : { hour : 6, min : 0 }, initials : "MB" },            
        { drugName : "Gabapentin",  route : ORAL,   dose : { value: 300.0,   unit : "mg" },   time : { hour : 6, min : 0 }, initials : "MB" },            
        { drugName : "Oxycodone",   route : ORAL,   dose : { value: 5.0,     unit : "mg" },   time : { hour : 9, min : 0 }, initials : "MB" },            
    ],
    logs : [
        {
            category : "radiology",
            entries : [
                //Fill in Radiology logs here. In the legacy patient scripts look for 'setInitialXrayResult('...) - see example below
                // { time : { hour : 9, min : 45 }, label : "¤Chest X-ray: There is no evidence of peumothorax. The heart is not enlarged. There are no signs of congestive heart failure. There are marked consolidation and collapse of the right lower lobe consistent with pneumonia. The mediastinum is normal." }
            ] 
        },
        {
            category : "12Lead",
            entries : [
                //Fill in 12-lead logs here. In the legacy patient scripts look for 'setInitial12LeadEcgResult('...) - see example below
                /*
                { time : { hour : 12, min : 30 }, label : "¤The ECG shows ST-T changes consistent with ischemia. There are occasional ventricular premature complexes. The heart rate is 120." },
                */
            ]
        },
        {
            category : "orders",
            entries : [
                //Fill in 12-lead logs here. In the legacy patient scripts look for 'setInitialOrders('...) - see example below
                { time : { hour : 9, min : 30 }, label : "¤Diet: regular[newline]Activity: ambulate with physical therapy[newline]Vital signs every 4 hours[newline]Transfuse two units packed red blood cells, starting at 100 mL/hr[newline]Recheck hemoglobin and hematocrit 1 hour after second unit[newline]Call results to attending provider[newline]Meds:[newline][tab]Acetaminophen 1 g PO every 8 hours[newline][tab]Gabapentin 300 mg PO every 8 hours[newline][tab]Oxycodone 5 mg PO every 4 hours PRN moderate pain level 4-7[newline][tab]Oxycodone 10 mg PO every 4 hours PRN severe pain level 8-10" }
            ]
        }
    ]
}